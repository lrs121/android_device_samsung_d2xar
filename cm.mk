$(call inherit-product, device/samsung/d2xar/full_d2xar.mk)

# Enhanced NFC
$(call inherit-product, vendor/cm/config/nfc_enhanced.mk)

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=d2xar \
    TARGET_DEVICE=d2xar \
    BUILD_FINGERPRINT="samsung/d2xar/d2xar:4.3/JSS15J/R530XWWUBMK4:user/release-keys" \
    PRIVATE_BUILD_DESC="d2xar-user 4.3 JSS15J R530XWWUBMK4 release-keys"

#PRODUCT_GMS_CLIENTID_BASE := android-verizon

PRODUCT_NAME := cm_d2xar
PRODUCT_DEVICE := d2xar

